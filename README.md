# rpl
Simple Perl REPL - a fancy eval

### Why?
I wanted a simple perl repl with minimal dependencies in one script

### Dependencies
* Term::ReadLine (a stub is enough to get started)

### Should have
* Either install `Data::Dump` (recommended) or `Data::Dumper`. The REPL does work without any of those but then pretty printing is basically useless.
* Getopt::Long for optional arguments

![](https://i.imgur.com/RiZkMZk.gif)

### What's there?
* Dumping of evaluated expressions (see "Should have")
* Basic multiline support
* Ability to require perl files with the §r command (just type `§r myfile.pl`)
* Strict (can be toggled by §strict and §no-strict - Variables are not strict since every command is in its own eval but things like barewords are caught)
* Code is evaluated in its own package 'REPL' so names don't clash with the main script functions (of course you can still override main functions if you try ;) )

#### Why have a `§r` command?
`§r <filename>` loads a perl file like require does. The difference is that it first removes the file if already loaded.
if you just do a `require` on a file that has already been required it won't reload the file.
